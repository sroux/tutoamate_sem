# TutoAMATE_SEM

Vous retrouverez toutes les données du tuto dans le dossier "*public*".

Les **bases de données** employées dans le dossier "*data*".
Les différents **scripts** ayant permis de simuler les données sont dans le dossier "*Ressources_tuto*".

Le script de la **présentation** est à la racine du dossier public et il se nomme *index.qmd*.

Enjoy ;)

*NB : L'intégralité des scripts a été conçu et rédigé par S. Roux*
